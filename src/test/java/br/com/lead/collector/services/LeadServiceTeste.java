package br.com.lead.collector.services;


import br.com.lead.collector.DTOs.CadastroDeLeadDTO;
import br.com.lead.collector.DTOs.IdProdutoDTO;
import br.com.lead.collector.models.Lead;
import br.com.lead.collector.models.Produto;
import br.com.lead.collector.repositories.LeadRepository;
import br.com.lead.collector.repositories.ProdutoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import javax.persistence.Id;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class LeadServiceTeste {

    //cria objetos vazios/manipulados das classes que a LeadService tem Dependencia
    @MockBean
    private LeadRepository leadRepository;

    @MockBean
    private ProdutoRepository produtoRepository;

    @Autowired
    private LeadService leadService;

    Lead lead;
    Produto produto;
    List<Produto> produtos;

    @BeforeEach
    private void setUp(){
        this.lead = new Lead();
        lead.setId(1);
        lead.setNome("Patricia");
        lead.setDataDeCadastro(LocalDate.now());
        lead.setTelefone("(11) 99999-9999");
        lead.setCpf("118.317.600-70");
        lead.setEmail("patricia@gmail.com");

        this.produto = new Produto();
        produto.setId(1);
        produto.setPreco(30.00);
        produto.setNome("Carteira");
        produto.setDescricao("Carteira de investimentos");

        List<Produto> produtos = Arrays.asList(produto);

        lead.setProdutos(produtos);


    }

    @Test
    public void testarBuscarLeadPeloId(){
        Optional<Lead> leadOptional = Optional.of(lead);
        Mockito.when(leadRepository.findById(Mockito.anyInt())).thenReturn(leadOptional);

        Lead leadObjeto = leadService.buscarLeadPeloId(8001);

        Assertions.assertEquals(lead, leadObjeto);
    }

    @Test
    public void buscarPorIdQueNaoExiste(){
        Optional<Lead> leadOptional = Optional.empty();
        Mockito.when(leadRepository.findById(Mockito.anyInt())).thenReturn(leadOptional);

        Assertions.assertThrows(RuntimeException.class, () -> {leadService.buscarLeadPeloId(4);});
    }


    @Test
    public void testarSalvarLead(){
        CadastroDeLeadDTO cadastroDeLeadDTO = new CadastroDeLeadDTO();
        cadastroDeLeadDTO.setCpf("386.286.640-84");
        cadastroDeLeadDTO.setEmail("patricia@gmail.com");
        cadastroDeLeadDTO.setNome("Patricia");
        cadastroDeLeadDTO.setTelefone("119999999");
        IdProdutoDTO idProdutoDTO = new IdProdutoDTO();
        idProdutoDTO.setId(2);

        List<IdProdutoDTO> idProdutoDTOS = Arrays.asList(idProdutoDTO);

        cadastroDeLeadDTO.setProdutos(idProdutoDTOS);

        Mockito.when(produtoRepository.findAllById(Mockito.anyIterable())).thenReturn(this.produtos);
        Mockito.when(leadRepository.save(Mockito.any(Lead.class))).then(objeto -> objeto.getArgument(0));

    }
}
