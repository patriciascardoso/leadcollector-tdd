package br.com.lead.collector.controllers;

import br.com.lead.collector.DTOs.CadastroDeLeadDTO;
import br.com.lead.collector.DTOs.IdProdutoDTO;
import br.com.lead.collector.DTOs.ResumoDeLeadDTO;
import br.com.lead.collector.models.Lead;
import br.com.lead.collector.models.Produto;
import br.com.lead.collector.services.LeadService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@WebMvcTest(LeadController.class)
public class LeadControllerTeste {

    @MockBean
    private LeadService leadService;

    @Autowired
    private MockMvc mockMvc;

    Lead lead;
    Produto produto;
    List<Produto> produtos;
    CadastroDeLeadDTO cadastroDeLeadDTO;

    @BeforeEach
    private void setUp(){
        this.lead = new Lead();
        lead.setId(1);
        lead.setNome("Patricia");
        lead.setDataDeCadastro(LocalDate.now());
        lead.setTelefone("(11) 99999-9999");
        lead.setCpf("118.317.600-70");
        lead.setEmail("patricia@gmail.com");

        this.produto = new Produto();
        produto.setId(1);
        produto.setPreco(30.00);
        produto.setNome("Carteira");
        produto.setDescricao("Carteira de investimentos");

        List<Produto> produtos = Arrays.asList(produto);

        lead.setProdutos(produtos);

        this.cadastroDeLeadDTO = new CadastroDeLeadDTO();
        cadastroDeLeadDTO.setTelefone("199991");
        cadastroDeLeadDTO.setNome("Xablau");
        cadastroDeLeadDTO.setEmail("xablau@gmail.com");
        cadastroDeLeadDTO.setCpf("118.317.600-70");

        List<IdProdutoDTO> idProdutoDTOS = new ArrayList<>();

        cadastroDeLeadDTO.setProdutos(idProdutoDTOS);

    }

    @Test
    public void testarBuscarPorID() throws Exception{
        Mockito.when(leadService.buscarLeadPeloId(1)).thenReturn(lead);

        mockMvc.perform(MockMvcRequestBuilders.get("/leads/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)));
    }

    @Test
    public void testarBuscarPorIDNaoExiste() throws Exception{
        Mockito.when(leadService.buscarLeadPeloId(Mockito.anyInt())).thenThrow(RuntimeException.class);

        mockMvc.perform(MockMvcRequestBuilders.get("/leads/" + lead.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }


    @Test
    public void cadastrarLead() throws Exception{
        Mockito.when(leadService.salvarLead(Mockito.any(CadastroDeLeadDTO.class))).thenReturn(lead);
        ObjectMapper objectMapper = new ObjectMapper();

        String leadJSON = objectMapper.writeValueAsString(cadastroDeLeadDTO);

        mockMvc.perform(MockMvcRequestBuilders.post("/leads")
                .contentType(MediaType.APPLICATION_JSON).content(leadJSON))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome", CoreMatchers.equalTo("Patricia")));
    }

    @Test
    public void testarValidacaoCadastroLead () throws Exception{
        cadastroDeLeadDTO.setEmail("@");

        ObjectMapper objectMapper = new ObjectMapper();
        String leadJSON = objectMapper.writeValueAsString(cadastroDeLeadDTO);

        mockMvc.perform(MockMvcRequestBuilders.post("/leads")
                .contentType(MediaType.APPLICATION_JSON).content(leadJSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());

        Mockito.verify(leadService, Mockito.times(0))
                .salvarLead(Mockito.any(CadastroDeLeadDTO.class));
    }
}
